package concurrent

import "fmt"

type Bird struct {
}

func Fly(birdNum int) {
	fmt.Println("===== bird", birdNum, "fly =====")
}
