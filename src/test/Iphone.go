package test

import "fmt"

type Iphone struct {
	price          float32
	productAddress string
}

func (iphone Iphone) Call() {
	fmt.Println("iphone call")
}
