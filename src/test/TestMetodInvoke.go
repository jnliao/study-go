package test

import "fmt"

func OuterMethodInvoke() {
	fmt.Println("OuterMethodInvoke")
}

func methodInvoke() {
	fmt.Println("testMethodInvoke")
}
