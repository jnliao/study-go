module go-test

go 1.16

require (
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	golang.org/x/sys v0.0.0-20211204120058-94396e421777 // indirect
)
